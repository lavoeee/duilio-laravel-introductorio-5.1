<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $loginPath = 'login';
    protected $redirectPath = 'home';
    protected $maxLoginAttempts = 2;
    protected $lockoutTime = 300;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getConfirmation', 'getLogout']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);

        $user->role = 'user';
        $user->registration_token = str_random(40);
        $user->save();

        $url = route('confirmation',['token' => $user->registration_token]);

        Mail::send('emails/registration', compact('user', 'url'), function ($message)  use($user){

            $message->from('john@johndoe.com', 'John Doe');
            $message->sender('john@johndoe.com', 'John Doe');
        
            $message->to($user->email, $user->name);
                
            $message->subject('Activa tu cuenta');
        });

        return $user;

    }

    protected function getFailedLoginMessage(){
        return trans('validation.login');
    }

    public function postRegister(Request $request)
    {
        // Se ejecuta despues de que el usuario se haya registrado
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        return redirect()->route('login')->with('alert' , 'Por favor confirma tu email: '.$user->email);
    }

    protected function getCredentials($request){
        return [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
           // 'registration_token' => null
        ];
    }

    public function getConfirmation($token){
        $user = User::where('registration_token', $token)->firstOrFail();
        $user->registration_token = null;
        $user->save();
        return redirect()->route('home')->with('alert' , 'Email confirmado!!');
    }

    protected function getLockoutErrorMessage($seconds)
    {
        $minutes = round($seconds / 60);
        return \Lang::has('auth.throttle')
            ? \Lang::get('auth.throttle', ['minutes' => $minutes])
            : 'Too many login attempts. Please try again in '.$seconds.' seconds.';
    }    

}
