<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Role
{
    protected $hierarchy = [
        'admin'     => 3,
        'editor'    => 2,
        'user'      => 1
    ];

    public function handle($request, Closure $next, $role){

        $user = Auth::User();

        if($this->hierarchy[$user->role] < $this->hierarchy[$role]):
            abort(404);
        endif;

        return $next($request);
    }
}
