@extends('layout')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">New Post</div>
				<div class="panel-body">
					<form action="{{ url('publish') }}" method="POST" class="form-horizontal">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label for="" class="col-md-4 control-label">Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="title" value="{{ old('title')}}">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-md-4 control-label">Post</label>
							<div class="col-md-6">
								<input type="textarea" class="form-control" name="post" value="{{ old('post')}}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
									Create Post
								</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
