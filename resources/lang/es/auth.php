<?php 
	return array(
		'errors_title' => 'Por favor corrige los siguientes errores',
		'login_title' => 'Inicio de sesión',
		'login_button' => 'Inicia sesión',
		'register_title' => 'Registro',
		'register_button' => 'Registrate !',
		'remember' => 'Recuérdame',
		'forgot_link' => 'Olvidaste tu contraseña ?',
		'throttle' => 'Demasiado intentos fallidos por favor intente de nuevo en :minutes minutos'
	);
?>